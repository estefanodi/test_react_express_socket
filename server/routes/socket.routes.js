const io = require("../index");
const {
  getConnectedUsers,
  createRoomIntent,
  responseToCreateRoom,
  play
} = require("../controllers/socket.controllers");

io.on("connection", socket => {
  socket.on("get_connected_users", getConnectedUsers.bind(null, io, socket));
  socket.on("create_room_intent", createRoomIntent.bind(null, io, socket));
  socket.on(
    "response_to_create_room",
    responseToCreateRoom.bind(null, io, socket)
  );
  socket.on("play", play.bind(null, io, socket));
});
