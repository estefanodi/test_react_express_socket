const express = require("express");
const port = process.env.port || 7070;
require("dotenv").config();
const app = express();
const http = require("http");
const server = http.createServer(app);
// =============== BODY PARSER SETTINGS ====================
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// =============== DATABASE CONNECTION =====================
const mongoose = require("mongoose");
mongoose.set("useCreateIndex", true);
async function connecting() {
  try {
    await mongoose.connect(process.env.MLAB_URL);
    console.log("Connected to the DB");
  } catch (error) {
    console.log(
      "ERROR: Seems like your DB is not running, please start it up !!!"
    );
  }
}
connecting();
//================ CORS ================================
const cors = require("cors");
app.use(cors());
//================ SOCKET ==============================
const socket = require("socket.io");
const io = socket(server);
module.exports = io;
// =============== ROUTES ==============================
app.use("/users", require("./routes/users.routes"));
require("./routes/socket.routes");
// =============== START SERVER =====================
server.listen(port, () => console.log(`server listening on port ${port}`));
