import React , { useState } from 'react'
import Axios from 'axios' 

const Login = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response = await Axios.post(`${process.env.REACT_APP_URL}/users/login`,{
				email:form.email,
				password:form.password
			})
          setMessage(response.data.message)
          if( response.data.ok ){

              setTimeout( ()=> {
				  props.login(response.data.token,response.data._id)
				  props.history.push('/users')
			  },2000)    
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
	         <label>Email</label>    
		     <input name="email"/>
		     <label>Password</label>
		     <input name="password"/>
		     <button>login</button>
		     <div className='message'><h4>{message}</h4></div>
	       </form>
}

export default Login










