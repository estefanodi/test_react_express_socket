import React, { useState, useEffect } from "react";
import axios from "axios";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import Login from "./containers/login.js";
import Register from "./containers/register.js";
import Users from "./containers/users.js";
import Navbar from "./components/navbar.js";
import Room from "./containers/room";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const token = JSON.parse(localStorage.getItem("token"));
  const verify_token = async () => {
    if (token === null) return setIsLoggedIn(false);
    try {
      axios.defaults.headers.common["Authorization"] = token;
      const response = await axios.post(
        `${process.env.REACT_APP_URL}/users/verify_token`
      );
      return response.data.ok
        ? (setIsLoggedIn(true),
          localStorage.setItem(
            "userId",
            JSON.stringify(response.data.succ._id)
          ))
        : setIsLoggedIn(false);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    verify_token();
  }, [verify_token]);

  const login = (token, userId) => {
    localStorage.setItem("token", JSON.stringify(token));
    localStorage.setItem("userId", JSON.stringify(userId));
    setIsLoggedIn(true);
  };
  const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("userId");
    setIsLoggedIn(false);
  };
  return (
    <Router>
      <Navbar isLoggedIn={isLoggedIn} logout={logout} />
      <Route
        exact
        path="/"
        render={props => {
          return isLoggedIn ? (
            <Redirect to={"/users"} />
          ) : (
            <Login login={login} {...props} />
          );
        }}
      />
      <Route
        path="/register"
        render={props => {
          return isLoggedIn ? (
            <Redirect to={"/users"} />
          ) : (
            <Register {...props} />
          );
        }}
      />
      <Route
        path="/users"
        render={props => {
          return !isLoggedIn ? (
            <Redirect to={"/"} />
          ) : (
            <Users logout={logout} {...props} />
          );
        }}
      />
      <Route
        exact
        path="/room"
        render={props => {
          return !isLoggedIn ? <Redirect to={"/"} /> : <Room {...props}/>;
        }}
      />
    </Router>
  );
}

export default App;
